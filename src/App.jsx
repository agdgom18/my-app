import './App.css';
import React from 'react';

function App() {
  const changTheme = () => {
    document.body.classList.toggle('dark');
    document.body.querySelector('ul').classList.toggle('green');
  };

  const titleElement = React.createElement(
    'h1',
    {
      style: { color: '#999', fontSize: '19px' },
    },
    'Solar system planets',
  );

  const planetList = (
    <ul className="planets-list">
      <li>Mercury</li>
      <li>Venus</li>
      <li>Earth</li>
      <li>Mars</li>
      <li>Saturn</li>
      <li>Uranus</li>
      <li>Neptune</li>
    </ul>
  );

  return (
    <div className="app">
      {titleElement}
      {planetList}
      <label className="switch" htmlFor="checkbox">
        <input onClick={changTheme} type="checkbox" id="checkbox" />
        <div className="slider round"></div>
      </label>
    </div>
  );
}

export default App;
